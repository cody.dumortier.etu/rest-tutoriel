package fr.ulille.iut.tva.ressource;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    
    @GET
    @PATH("valeur/{niveauTVA}")
    public double getValeurTauxParDefaut(@PathParam("niveauTVA") String niveau) {
    	try {
    	return TauxTva.valueOf((niveau.toUpperCase())).taux;
    }
    	catch(Exception e) {
    		throw new NiveauTvaInexistantException();
    	}
    }
    
    @GET
    @PATH("reduit?{somme}")
    public double getMontantTotal(@PathParam("somme") double somme) {
    	try {
    	return TauxTva.REDUIT.taux * somme;
    }
    	catch (Exception e){
    		throw new NiveauTvaInexistantException();
    	}
    }
}
