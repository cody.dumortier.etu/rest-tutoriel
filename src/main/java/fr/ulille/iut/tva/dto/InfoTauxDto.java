package fr.ulille.iut.tva.dto;
import java.util.ArrayList;
import java.util.List;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InfoTauxDto {
    private String label;
    private double taux;
    
    public InfoTauxDto() {
    	
    }

    public InfoTauxDto(String label, double taux) {
        this.label = label;
        this.taux = taux;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }
    
    @GET
    @Path("details/{taux}{montant}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public String getDetail(@PathParam("taux") InfoTauxDto taux, @PathParam("montant") double montant) {
    	String res = "montantTotal :" + montant + ", montantTva :" + taux.getTaux() * montant + ", somme :" + 
    (montant - taux.getTaux() * montant) + ", tauxLabel :" + taux.getLabel() + ", tauxValue :" + taux.getTaux();
    	
    	return res;
    }
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }

}
